"use strict";
const ccxt = require('ccxt');
// float calculation leads to
var Decimal = require('decimal.js');

// this is required, else pkg wont include them and this resolve into errors
require('ethereumjs-abi');
require('ethereumjs-util');

// Database
const {AsyncNedb} = require('nedb-async');
const orderDB = new AsyncNedb({filename: 'orders', autoload: true});
const gridDB = new AsyncNedb({filename: 'grids', autoload: true});

// Used for user input
const readline = require('readline-sync');

// Strategy uses limit orders
const type = 'limit';

let coss;
let userId;
let privateKey;

let symbol;
let market;
let orders;


let loader;

(async () => {
    orderDB.persistence.compactDatafile();
    gridDB.persistence.compactDatafile();

    console.log('Welcome!');

    // User prompt for Api credentials
    await credentials();

    // Market selection to trade on
    await selectMarket();

    // This will ask the user if he wants to continue or start a new setup
    await loadOrders();


})();

async function credentials() {
    console.log('Please enter your credentials ... this wont be saved and needs to be inserted each time');

    userId = await readline.question('Enter your userid: ');
    privateKey = await readline.question('Enter your privateKey: ');

    coss = new ccxt.cossdex(
        {
            'apiKey': userId,
            'secret': privateKey
        }
    );

    console.clear();
}

async function selectMarket() {
    console.log('Loading markets ...');

    const markets = await coss.fetchMarkets();
    symbol = readline.question('Type the market you want to trade \nAvailable:\n' + markets.reduce((prev, curr) => {
        return (prev.symbol ? prev.symbol : prev) + ', ' + curr.symbol
    }) + '\n');

    market = markets.find(market => market.symbol === symbol);

    if (!market) {
        console.log('Unknown market ... try again');
        // repeat and wait until something correct is selected
        await selectMarket();
    }
}

async function loadOrders() {
    console.log('Checking Database ...');
    orders = await orderDB.asyncFind({symbol: symbol, user: userId});

    if (orders.length > 0) {
        console.log('Found ' + orders.length + ' old orders on this pair!');

        const continueOrCancel = await readline.question('Continue (y) or cancel all orders and create new (n): ');
        if (continueOrCancel === 'y') {
            checkOrders(symbol, userId);
        } else {
            cancelOrders();
        }
    } else {
        console.log('No old orders found, you are good to go!');
        setup();
    }
}

async function cancelOrders() {
    console.log('Cancelling ' + orders.length + ' orders ...');
    let i = 0;
    for (let order of orders) {
        try {
            const canceled = await coss.cancelOrder(order.id, order.symbol);
            if (canceled.status === 'canceled') {
                await orderDB.asyncRemove({id: order.id, user: userId});
            }
            console.log(++i + ' canceled');
        } catch (e) {
            console.log('Failed to cancel order with id: ' + order.id);
        }
    }

    if (i === orders.length) {
        await gridDB.asyncRemove({symbol: symbol, user: userId});
        orderDB.persistence.compactDatafile();
        gridDB.persistence.compactDatafile();

        console.log('Making sure everything is clean ...');
        loadOrders();
    } else {
        console.log('Failed to cancel ' + (orders.length - i) + ' orders. Retrying ...');
        cancelOrders();
    }
}

async function setup() {
    const upperWall = readline.question('Please enter the upper price in which you want to trade: ');
    const lowerWall = readline.question('Please enter the lower price in which you want to trade: ');

    if (upperWall < lowerWall) {
        console.log('That makes no sense. Try again');

        setup();
        return;
    }

    const numberOfGrids = readline.question('Enter the number of grids: ');
    const amount = readline.question('Enter the amount per grid (max precision: ' + market.precision.amount + '): ');

    console.log('Thats all! Fetching Orderbook ...');
    const book = await coss.fetchOrderBook(symbol);
    const currentSell = book.asks[0][0];
    const currentBuy = book.bids[0][0];
    const center = div(add(currentSell, currentBuy), 2);
    console.log('Looks like the current ask is: ' + currentSell + ' and the current bid is: ' + currentBuy + ' that means the center is: ' + center);

    console.log('Building grids with all the information ...');
    let grids = buildGrids(upperWall, lowerWall, numberOfGrids);
    await gridDB.asyncUpdate({symbol: symbol, user: userId}, {symbol, user: userId, grids}, {upsert: true});
    const closest = grids.reduce((prev, curr) => (Math.abs(curr - center) < Math.abs(prev - center) ? curr : prev));
    grids.splice(grids.indexOf(closest), 1);

    console.log('Done! Calculating needed balance ...');

    let currency1 = 0;
    let currency2 = 0;

    for (let grid of grids) {
        if (grid > center) {
            currency1 = add(currency1, amount);
        } else {
            currency2 = add(currency2, mul(grid, amount))
        }
    }

    console.log('You will need ' + currency1 + symbol.split('/')[0] + ' and ' + currency2 + symbol.split('/')[1] + ' !');
    console.log('Checking balance ...');

    const balance = await coss.fetchFreeBalance();
    let faulty = false;
    if (balance[symbol.split('/')[0]] < currency1) {
        console.log('Oops! Looks like you are missing ' + sub(currency1, balance[symbol.split('/')[0]]) + symbol.split('/')[0] + '!');
        faulty = true;
    }
    if (balance[symbol.split('/')[1]] < currency2) {
        console.log('Oops! Looks like you are missing ' + sub(currency2, balance[symbol.split('/')[1]]) + symbol.split('/')[1] + '!');
        faulty = true;
    }
    if (!faulty) {
        console.log('Congrats you have enough balance!');

        const create = readline.question('Create orders? (y/n): ');
        if (create === 'y') {
            createOrders(grids, center, amount);
        } else {
            setup();
        }
    } else {
        setup();
    }
}

async function createOrders(grids, center, amount) {
    console.log('Creating orders ...');

    for (let grid of grids) {
        console.log('Creating order at pricelevel: ' + grid);
        try {
            const order = await coss.createOrder(symbol, type, grid > center ? 'sell' : 'buy', amount, grid);
            orderDB.insert({
                id: order.id,
                symbol,
                type,
                side: grid > center ? 'sell' : 'buy',
                amount,
                grid,
                user: userId
            });
        } catch (e) {
            console.log('Failed to create order ... Will retry after 5 sec');
            await sleep(5000);

            try {
                const order = await coss.createOrder(symbol, type, grid > center ? 'sell' : 'buy', amount, grid);
                orderDB.insert({
                    id: order.id,
                    symbol,
                    type,
                    side: grid > center ? 'sell' : 'buy',
                    amount,
                    grid,
                    user: userId
                });

                console.log('Looks like it worked this time! Continuing ...');
            } catch (e) {
                console.log('Seems like something is wrong :( will abort and cancel all orders ...');
                await cancelOrders();
                setup();
                return;
            }
        }
    }

    console.log('Created ' + grids.length + ' orders!');

    console.log('Will now check every 10 seconds for a change ...');
    checkOrders(symbol, userId);
}

async function checkOrders(symbol, user) {
    if (!loader) {
        console.log('Checking orders for symbol ' + symbol + ' ...');
        startSpinner();
    }

    const orders = await orderDB.asyncFind({symbol: symbol, user: user});
    let closedOrders;
    try {
        closedOrders = await coss.fetchClosedOrders(symbol, undefined, 100);
    } catch (e) {
        console.log('Failed to fetch closed orders ... will retry next cycle!');
        await sleep(10000);
        checkOrders(symbol, user);
        return;
    }

    for (let closedOrder of closedOrders) {
        for (let order of orders) {
            if (closedOrder.id === order.id) {
                stopSpinner();

                console.log(order.side + ' order was filled at price: ' + order.grid);

                const wrappedGrids = await gridDB.asyncFindOne({symbol: symbol, user: user});
                const grids = wrappedGrids.grids;
                let index = grids.indexOf(order.grid);

                let newSide = '';
                if (order.side === 'buy') {
                    index--;
                    newSide = 'sell';
                    console.log('Creating sell order at pricelevel: ' + grids[index]);
                } else {
                    index++;
                    newSide = 'buy';
                    console.log('Creating buy order at pricelevel: ' + grids[index]);
                }
                try {
                    const newOrder = await coss.createOrder(symbol, type, newSide, order.amount, grids[index]);
                    await orderDB.insert({
                        id: newOrder.id,
                        symbol,
                        type,
                        side: newSide,
                        amount: order.amount,
                        grid: grids[index],
                        user
                    });
                    await orderDB.asyncRemove({id: order.id});
                } catch (e) {
                    console.log('Something went wrong ... will retry next cycle!');
                }
            }
        }
    }
    await sleep(10000);
    checkOrders(symbol, user);
}

function startSpinner() {
    const P = ['\\', '|', '/', '-'];
    let x = 0;
    loader = setInterval(() => {
        process.stdout.write(`\r${P[x++]}`);
        x %= P.length;
    }, 250);
}

function stopSpinner() {
    clearInterval(loader);
}

function sleep(ms) {
    return new Promise((resolve) => {
        setTimeout(resolve, ms);
    });
}

function add(a, b) {
    a = new Decimal(a);
    b = new Decimal(b);

    return a.add(b);
}

function sub(a, b) {
    a = new Decimal(a);
    b = new Decimal(b);

    return a.sub(b);
}

function mul(a, b) {
    a = new Decimal(a);
    b = new Decimal(b);

    return a.mul(b);
}

function div(a, b) {
    a = new Decimal(a);
    b = new Decimal(b);

    return a.div(b);
}

function pow(a, pow) {
    a = new Decimal(a);
    return a.pow(pow);
}

function buildGrids(upperWall, lowerWall, numberOfGrids, ) {

    // Grids
    let grids = [];
    let percentage = getGridDistance(upperWall, lowerWall, numberOfGrids);
    console.log('You will make around ' +  mul(sub(1, percentage.toFixed(4)),100) + '% (- fees) profit per completed grid');
    let current = upperWall;
    for (let i = 0; i < numberOfGrids; i++) {
        grids.push(coss.decimalToPrecision(current, 0, market.precision.price));
        current = mul(current, percentage);
    }
    return grids;
}

function getGridDistance(upperWall, lowerWall, numberOfGrids) {
    upperWall = new Decimal(upperWall);
    lowerWall = new Decimal(lowerWall);

    return pow(div(lowerWall, upperWall), div(1, numberOfGrids - 1));
}
