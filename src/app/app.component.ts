import {Component} from '@angular/core';
import {ElectronService} from './core/services';
import {TranslateService} from '@ngx-translate/core';
import {BotsService} from './shared/services/bots.service';
import {Bot} from './shared/services/bots.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  bots: Bot[] = [];

  constructor(
    private electronService: ElectronService,
    private translate: TranslateService,
    private botsService: BotsService
  ) {
    this.translate.setDefaultLang('en');

    this.botsService.getBots().subscribe(bots => {
      this.bots = bots;
    })

  }

}
