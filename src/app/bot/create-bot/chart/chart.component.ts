import {Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges} from '@angular/core';
import {Market, OHLCV, Order} from 'ccxt';

import {
  ApexAxisChartSeries,
  ApexChart,
  ApexGrid,
  ApexLegend,
  ApexStroke,
  ApexTitleSubtitle,
  ApexTooltip,
  ApexXAxis,
  ApexYAxis,
} from 'ng-apexcharts';
import {CossApiService} from '../../../shared/services/coss-api.service';
import {take} from 'rxjs/operators';

export type ChartOptions = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  xaxis: ApexXAxis;
  yaxis: ApexYAxis;
  title: ApexTitleSubtitle;
  tooltip: ApexTooltip;
  stroke: ApexStroke;
  grid: ApexGrid;
  legend: ApexLegend;
};

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss']
})
export class ChartComponent implements OnInit, OnChanges, OnDestroy {

  @Input()
  market: Market;

  @Input()
  timeframe: string = '1d';

  @Input()
  orders: Array<Partial<Order>>;

  @Input()
  subtitle = '';

  chartOptions: Partial<ChartOptions> = {
    series: [],
    chart: {
      height: 500,
      width: '100%',
      type: 'candlestick',
      animations: {
        enabled: false
      }
    },
    stroke: {
      width: 1
    },
    xaxis: {
      type: "datetime"
    },
    yaxis: {
      forceNiceScale: true
    },
    legend: {
      show: false
    },
    tooltip: {
      enabled: false
    }
  };
  dateTimeFrom;
  dateTimeTo;
  candleStickSeries: ApexAxisChartSeries = [];

  ordersSeries: ApexAxisChartSeries = [];

  colors = ['black'];

  interval;

  constructor(private dex: CossApiService) {
  }

  ngOnInit(): void {
    if (this.interval) {
      clearInterval();
    }

    this.fetchOHLCV();
    if (this.timeframe !== '1d') {
      this.interval = setInterval(() => {
        this.fetchOHLCV();
      }, 30000);
    }
  }

  fetchOHLCV() {
    this.dex.fetchOHLCV(this.market.symbol, this.timeframe).pipe(take(1)).subscribe(ohlcv => {
      this.convertOHLCVToSeries(ohlcv);
    })
  }

  convertOHLCVToSeries(ohlcv: OHLCV[]) {
    const data = [];
    const dateTimes = [];

    ohlcv.forEach(entry => {
      const dataEntry = {
        x: new Date(entry[0]),
        y: [
          entry[1],
          entry[2],
          entry[3],
          entry[4],
        ]
      };
      data.push(dataEntry);
      dateTimes.push(entry[0]);
    });

    this.dateTimeFrom = dateTimes[0];
    this.dateTimeTo = dateTimes[dateTimes.length - 1];

    this.candleStickSeries = [{
      name: 'candle',
      type: 'candlestick',
      data: data
    }];

    this.ordersToSeries();
  }

  ordersToSeries() {
    const series: ApexAxisChartSeries = [];
    const colors = [];

    if (this.orders) {
      this.orders.forEach((order, index) => {

        const array = [];
        array.push({
          x: new Date(this.dateTimeFrom),
          y: order.price
        });
        array.push({
          x: new Date(this.dateTimeTo),
          y: order.price
        });

        series.push({
          name: 'grid #' + (index + 1),
          type: 'line',
          data: array
        });

        if (order.side === 'sell') {
          colors.push('#fa5252');
        } else {
          colors.push('#10b885');
        }
      });
    }

    this.ordersSeries = series;
    this.colors = ['black'].concat(colors);

    this.updateChart();
  }

  updateChart() {
    this.chartOptions.chart.type = this.ordersSeries.length > 0 ? 'line' : 'candlestick';
    this.chartOptions.series = this.candleStickSeries.concat(this.ordersSeries);
  }

  ngOnDestroy(): void {
    if (this.interval) {
      clearInterval(this.interval);
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.orders && this.candleStickSeries.length > 0) {
      this.ordersToSeries();
    }

    if (changes.timeframe && changes.timeframe.previousValue !== changes.timeframe.currentValue) {
      if (this.interval) {
        clearInterval();
      }

      this.fetchOHLCV();
      if (this.timeframe !== '1d') {
        this.interval = setInterval(() => {
          this.fetchOHLCV();
        }, 30000);
      }
    }

  }

}
