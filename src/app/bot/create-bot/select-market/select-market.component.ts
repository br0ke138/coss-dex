import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Market} from 'ccxt';
import {CossApiService} from '../../../shared/services/coss-api.service';
import {take} from 'rxjs/operators';
import {BotsService} from '../../../shared/services/bots.service';

@Component({
  selector: 'app-select-market',
  templateUrl: './select-market.component.html',
  styleUrls: ['./select-market.component.scss']
})
export class SelectMarketComponent implements OnInit {

  markets: Array<Market>;

  @Output()
  selectMarket: EventEmitter<Market> = new EventEmitter<Market>();

  constructor(private dex: CossApiService, private bots: BotsService) {
  }

  ngOnInit(): void {
    this.dex.fetchMarkets().pipe(take(1)).subscribe(markets => {
      this.bots.getBots().pipe(take(1)).subscribe(bots => {
        const notUsed: Array<Market> = [];
        markets.forEach(market => {
          let found = false;
          bots.forEach(bot => {
            if (bot.symbol === market.symbol) {
              found = true;
            }
          });
          if (!found) {
            notUsed.push(market);
          }
        });
        this.markets = notUsed;
      });

    })
  }

}
