import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {Balances, Market, Order, OrderBook, Trade} from 'ccxt';
import {CossApiService} from '../../shared/services/coss-api.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Decimal} from 'decimal.js';
import {BotsService} from '../../shared/services/bots.service';
import {debounceTime, take} from 'rxjs/operators';
import {BaseQuote, Bot, History} from '../../shared/services/bots.model';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-create-bot',
  templateUrl: './create-bot.component.html',
  styleUrls: ['./create-bot.component.scss']
})
export class CreateBotComponent implements OnInit, OnDestroy {

  @Input()
  markets: Array<Market>;

  @Output()
  close: EventEmitter<any> = new EventEmitter<any>();

  bot: Bot;

  balances: Balances;
  grids: Array<string>;
  selectedMarket: Market;

  accountForm: FormGroup = new FormGroup({
    apiKey: new FormControl('', [Validators.required]),
    secret: new FormControl('', [Validators.required]),
  });

  botForm: FormGroup = new FormGroup({
    upperWall: new FormControl(null, [Validators.required]),
    lowerWall: new FormControl(null, [Validators.required]),
    center: new FormControl(null, [Validators.required]),
    amount: new FormControl(null, [Validators.required]),
    numberOfGrids: new FormControl(null, [Validators.required, Validators.min(2), Validators.max(500)]),
  });

  botFormSub;
  accountFormSub;
  routeParamSub;

  orderBook: OrderBook;
  gridOrders: Array<Partial<Order>> = [];
  baseQuote: BaseQuote = {
    base: 0,
    quote: 0
  };

  profit: BaseQuote = {
    base: 0,
    quote: 0
  };

  interval;

  history: History;
  inOrders: BaseQuote;

  constructor(private dex: CossApiService, private botsService: BotsService, private route: ActivatedRoute, private router: Router) {
  }

  ngOnInit(): void {
    this.routeParamSub = this.route.params.subscribe(params => {
      this.unsubscribeFromForms();
      if (params['id']) {
        this.botsService.getBot(params['id']).pipe(take(1)).subscribe(bot => {
          this.bot = bot;
          this.poll();
          this.dex.fetchMarkets().pipe(take(1)).subscribe(markets => {
            markets.forEach(market => {
              if (market.symbol === bot.symbol) {
                this.selectedMarket = market;
              }
            });
            this.subscribeToForms();
            this.accountForm.patchValue(this.bot.account);
            this.botForm.patchValue({
              upperWall: this.bot.upperWall,
              lowerWall: this.bot.lowerWall,
              center: this.bot.center,
              amount: this.bot.amount,
              numberOfGrids: this.bot.numberOfGrids,
            })
          });

        });
      } else {
        this.reset();
        this.subscribeToForms();
      }
    });

  }

  subscribeToForms() {
    this.accountFormSub = this.accountForm.get('apiKey')
      .valueChanges
      .pipe(debounceTime(1000))
      .subscribe(apiKey => {
        console.log(apiKey);
        if (apiKey !== '' && apiKey !== null) {
          this.dex.fetchBalance({apiKey: apiKey, secret: ''}).pipe(take(1)).subscribe(bal => {
            this.balances = bal;
          }, error => {
            console.log(error, 'will retry after 2 sec');
            setTimeout(() => {
              this.dex.fetchBalance({apiKey: apiKey, secret: ''}).pipe(take(1)).subscribe(bal => {
                this.balances = bal;
              }, error => {

              });
            }, 2000);
          });
        } else {
          this.balances = null;
        }
      });

    this.botFormSub = this.botForm.valueChanges.pipe(debounceTime(1000)).subscribe(formChange => {
      if (this.botForm.valid && this.botForm.get('upperWall').value > this.botForm.get('lowerWall').value) {
        this.calculateGrids();
      }
    });
  }

  unsubscribeFromForms() {
    if (this.accountFormSub) {
      this.accountFormSub.unsubscribe();
    }
    if (this.botFormSub) {
      this.botFormSub.unsubscribe();
    }
  }

  save() {
    let bot: Bot;

    if (this.bot) {
      this.bot = {
        ...this.bot,
        upperWall: this.botForm.get('upperWall').value,
        lowerWall: this.botForm.get('lowerWall').value,
        center: this.botForm.get('center').value,
        amount: this.botForm.get('amount').value,
        numberOfGrids: this.botForm.get('numberOfGrids').value,
        grids: this.grids,
        profit: (1 - this.getGridDistance()) * 100,
        account: {
          apiKey: this.accountForm.get('apiKey').value,
          secret: this.accountForm.get('secret').value
        }};

        bot = this.bot;
    } else {
      bot = {
        symbol: this.selectedMarket.symbol,
        upperWall: this.botForm.get('upperWall').value,
        lowerWall: this.botForm.get('lowerWall').value,
        center: this.botForm.get('center').value,
        amount: this.botForm.get('amount').value,
        numberOfGrids: this.botForm.get('numberOfGrids').value,
        grids: this.grids,
        orders: [],
        status: 'stopped',
        profit: (1 - this.getGridDistance()) * 100,
        account: {
          apiKey: this.accountForm.get('apiKey').value,
          secret: this.accountForm.get('secret').value
        }
      };
    }

    this.botsService.saveBot(bot).pipe(take(1)).subscribe(savedBot => {
      this.router.navigate(['/bot/' + savedBot._id]);
    });
  }

  reset() {
    this.accountForm.reset();
    this.botForm.reset();
    this.selectedMarket = null;
    this.bot = null;
  }

  delete() {
    this.botsService.delete(this.bot);
    this.router.navigate(['/bot']);
  }

  calculateGrids() {
    const upperWall = new Decimal(this.botForm.get('upperWall').value);
    const lowerWall = new Decimal(this.botForm.get('lowerWall').value);
    const numberOfGrids = new Decimal(this.botForm.get('numberOfGrids').value);

    this.grids = this.buildGrids(upperWall, lowerWall, numberOfGrids);
    this.convertGridsToOrders();
  }

  convertGridsToOrders() {
    let center = this.botForm.get('center').value;
    let amount = this.botForm.get('amount').value;

    let grids = [].concat(this.grids);
    const closest = grids.reduce((prev, curr) => (Math.abs(this.sub(curr, center)) < Math.abs(this.sub(prev, center)) ? curr : prev));
    grids.splice(grids.indexOf(closest), 1);

    let orders: Array<Partial<Order>> = [];
    let base = 0;
    let quote = 0;

    grids.forEach(grid => {
      if (grid > center) {
        orders.push({
          price: grid,
          amount,
          side: 'sell'
        });
        base = this.add(base, amount);
      } else {
        orders.push({
          price: grid,
          amount,
          side: 'buy'
        });
        quote = this.add(quote, amount * grid);
      }
    });

    this.gridOrders = orders;

    this.baseQuote = {
      base,
      quote
    }
  }

  buildGrids(upperWall, lowerWall, numberOfGrids,) {

    // Grids
    let grids = [];
    let percentage = this.getGridDistance();
    let current = upperWall;
    for (let i = 0; i < numberOfGrids; i++) {
      grids.push(this.dex.decimalToPrecision(current, 0, this.selectedMarket.precision.price));
      current = this.mul(current, percentage);
    }
    return grids;
  }

  getGridDistance() {
    const upperWall = new Decimal(this.botForm.get('upperWall').value);
    const lowerWall = new Decimal(this.botForm.get('lowerWall').value);
    const numberOfGrids = new Decimal(this.botForm.get('numberOfGrids').value);

    return this.pow(this.div(lowerWall, upperWall), this.div(1, this.sub(numberOfGrids, 1)));
  }

  add(a, b) {
    a = new Decimal(a);
    b = new Decimal(b);

    return a.add(b);
  }

  sub(a, b) {
    a = new Decimal(a);
    b = new Decimal(b);

    return a.sub(b);
  }

  mul(a, b) {
    a = new Decimal(a);
    b = new Decimal(b);

    return a.mul(b);
  }

  div(a, b) {
    a = new Decimal(a);
    b = new Decimal(b);

    return a.div(b);
  }

  pow(a, pow) {
    a = new Decimal(a);
    return a.pow(pow);
  }

  selectMarket(market: Market) {
    this.selectedMarket = market;
  }

  fetchBotUpdates() {
    this.botsService.getBot(this.bot._id).pipe(take(1)).subscribe(bot => {
      this.bot = bot;
      this.gridOrders = bot.orders.sort(function (a, b) {
        return (a.price > b.price) ? -1 : ((b.price > a.price) ? 1 : 0);
      });
      if (bot.status === 'stopped') {
        this.botForm.enable({ emitEvent: false });
        this.accountForm.enable({ emitEvent: false });
        this.history = null;
        this.inOrders = null;
        if (this.interval) {
          clearInterval(this.interval);
        }
      } else {
        this.botsService.getHistory(bot.history_id).pipe(take(1)).subscribe(history => {
          this.history = history;
          const baseQuote: BaseQuote = {
            base: 0,
            quote: 0
          };
          history.completedOrders.forEach(order => {
            if (order.amount !== bot.amount) {
              if (order.side === 'sell') {
                let profit = bot.amount - order.amount;
                baseQuote.base = baseQuote.base + profit;
                baseQuote.quote = baseQuote.quote + (profit * order.price);
              } else {
                let profit = order.amount - bot.amount;
                baseQuote.base = baseQuote.base + profit;
                baseQuote.quote = baseQuote.quote + (profit * order.price);
              }
            }
          });
          this.profit = baseQuote;
        });

        const inOrders: BaseQuote = {
          base: 0,
          quote: 0
        };

        bot.orders.forEach(order => {
          if (order.side === 'sell') {
            inOrders.base = this.botsService.add(inOrders.base, order.amount).toNumber();
          } else {
            inOrders.quote = this.botsService.add(inOrders.quote, this.botsService.mul(order.amount, order.price)).toNumber();
          }
        });
        this.inOrders = inOrders;
        this.botForm.disable({ emitEvent: false });
        this.accountForm.disable({ emitEvent: false });
      }
    })
  }

  start() {
    this.botsService.startBot(this.bot);
    this.poll();
  }

  stop() {
    this.botsService.stopBot(this.bot);
    this.poll();
  }

  pause() {
    this.botsService.pause(this.bot);
    this.poll();
  }

  continue() {
    this.botsService.continue(this.bot);
    this.poll();
  }

  poll() {
    if (this.interval) {
      clearInterval(this.interval);
    }
    this.fetchBotUpdates();
    this.interval = setInterval(() => {
      this.fetchBotUpdates();
    }, 60000);
  }

  ngOnDestroy(): void {
    this.unsubscribeFromForms();
    this.routeParamSub.unsubscribe();
  }

}
