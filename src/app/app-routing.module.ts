import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PageNotFoundComponent} from './shared/components';
import {DustConverterComponent} from './dust-converter/dust-converter.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {CreateBotComponent} from './bot/create-bot/create-bot.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full'
  },
  {
    path: 'dashboard',
    component: DashboardComponent
  },
  {
    path: 'bot/:id',
    component: CreateBotComponent
  },
  {
    path: 'bot',
    component: CreateBotComponent
  },
  {
    path: 'dust',
    component: DustConverterComponent
  },
  {
    path: '**',
    component: PageNotFoundComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {relativeLinkResolution: 'legacy'}),
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
