import {Order} from "ccxt";


export interface Bot {
  _id?: string;
  history_id?: string;
  symbol: string;
  upperWall: number;
  lowerWall: number;
  center: number;
  amount: number;
  numberOfGrids: number;
  grids: Array<string>,
  orders: Array<Order>,
  status: 'running' | 'stopped' | 'paused' | 'stopping' | 'creating' | 'error',
  profit: number,
  account: ApiAccount,
}

export interface ApiAccount {
  'apiKey': string,
  'secret': string,
}

export interface History {
  _id?: string;
  bot_id: string;
  symbol: string;
  initialInvest: BaseQuote;
  currentInvest: BaseQuote;
  fee: BaseQuote;
  completedOrders: Array<Order>;
}

export interface BaseQuote {
  base: number;
  quote: number;
}
