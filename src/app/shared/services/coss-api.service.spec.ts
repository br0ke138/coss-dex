import { TestBed } from '@angular/core/testing';

import { CossApiService } from './coss-api.service';

describe('CossApiService', () => {
  let service: CossApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CossApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
