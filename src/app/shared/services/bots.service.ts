import {Injectable} from '@angular/core';
import {ElectronService} from '../../core/services';
import {Observable, Subject} from 'rxjs';
import * as Datastore from 'nedb';
import {Bot, History} from './bots.model';
import {CossApiService} from './coss-api.service';
import {Decimal} from 'decimal.js';
import {take} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BotsService {

  bots: Datastore = new Datastore({filename: 'bots', autoload: true});
  history: Datastore = new Datastore({filename: 'history', autoload: true});
  subject: Subject<Bot[]> = new Subject();
  intervals = {};

  checkInterval = 30000;

  constructor(private electronService: ElectronService, private dex: CossApiService) {
    // this.clearOrders('COS/USDT');
    this.init();
  }

  init() {
    // this.bots.find({status: 'error'}, (err, bots: Bot[]) => {
    //   bots.forEach(bot => {
    //     this.dex.fetchOpenOrders(bot.symbol, bot.account).subscribe(openOrders => {
    //       bot.orders = openOrders;
    //       bot.status = 'running';
    //       this.updateBot(bot);
    //     })
    //   })
    // });
    this.bots.find({status: 'creating'}, (err, bots: Bot[]) => {
      bots.forEach(bot => {
        this.stopBot(bot);
      })
    });
    this.bots.find({status: 'stopping'}, (err, bots: Bot[]) => {
      bots.forEach(bot => {
        this.stopBot(bot);
      })
    });
    this.bots.find({status: 'running'}, (err, bots: Bot[]) => {
      if (err) console.log(err);
      bots.forEach(bot => {
        if (bot.grids.length === bot.orders.length + 1) {
          this.continue(bot);
        } else {
          bot.status = 'error';
          this.updateBot(bot);
        }
      })
    });
  }

  pause(bot: Bot) {
    clearInterval(this.intervals[bot._id]);
    bot.status = 'paused';
    this.updateBot(bot);
  }

  continue(bot: Bot) {
    this.intervals[bot._id] = setInterval(() => {
      this.checkOrders(bot);
    }, this.checkInterval);

    bot.status = 'running';
    this.updateBot(bot);
  }

  startBot(bot: Bot) {
    bot.status = 'creating';
    this.updateBot(bot);

    this.createNewHistoryForBot(bot).pipe(take(1)).subscribe(bot => {
      this.checkBalance(bot).pipe(take(1)).subscribe(() => {
        this.createOrders(bot);
      }, err => {
        bot.status = 'stopped';
        this.updateBot(bot);
        alert(err);
      })
    });
  }

  checkBalance(bot) {
    return new Observable(subscriber => {
      this.dex.fetchBalance(bot.account).pipe(take(1)).subscribe(balances => {
        const currencies = bot.symbol.split('/');

        let grids = [].concat(bot.grids);
        const closest = grids.reduce((prev, curr) => (Math.abs(this.sub(curr, bot.center)) < Math.abs(this.sub(prev, bot.center)) ? curr : prev));
        grids.splice(grids.indexOf(closest), 1);

        let quote = new Decimal(0);
        let base = new Decimal(0);

        grids.forEach(grid => {
          if (grid > bot.center) {
            quote = quote.add(bot.amount)
          } else {
            base = base.add(this.mul(grid, bot.amount))
          }
        });

        console.log(balances[currencies[0]].free, quote.toNumber(), balances[currencies[0]].free < quote.toNumber());
        console.log(balances[currencies[1]].free, base.toNumber(), balances[currencies[1]].free < base.toNumber());

        if (balances[currencies[0]].free < quote.toNumber()) {
          subscriber.error('Not enough ' + currencies[0]);
        } else if (balances[currencies[0]].free < base.toNumber()) {
          subscriber.error('Not enough ' + currencies[1]);
        } else {
          subscriber.next();
        }
        subscriber.complete();
      })
    })
  }

  checkOrders(bot: Bot) {
    this.dex.fetchClosedOrders(bot.symbol, bot.account).pipe(take(1)).subscribe(closedOrders => {
      closedOrders.forEach(closedOrder => {
        bot.orders.forEach(order => {
          if (closedOrder.id === order.id) {

            let index = bot.grids.indexOf(closedOrder.price.toString());

            let newSide = '';
            let newAmount = bot.amount;

            if (closedOrder.side === 'buy') {
              index--;
              newSide = 'sell';
            } else {
              index++;
              newSide = 'buy';
            }

            if (bot.amount === closedOrder.amount) {
              newAmount = this.add(closedOrder.amount, this.div(this.sub(this.mul(closedOrder.price, closedOrder.amount), this.mul(bot.grids[index], closedOrder.amount)), this.mul(2, bot.grids[index]))).toNumber();
            }

            this.dex.createOrder(bot.symbol, 'limit', newSide, newAmount, bot.grids[index], bot.account).pipe(take(1)).subscribe(order => {
              // @ts-ignore
              order.side = newSide;
              order.price = parseFloat(bot.grids[index]);
              order.amount = newAmount;
              order.symbol = bot.symbol;

              bot.orders.push(order);
              bot.orders = bot.orders.filter(entry => {
                return entry.id !== closedOrder.id;
              });
              this.updateBot(bot);

              this.history.update({_id: bot.history_id}, {
                $push: {
                  completedOrders: closedOrder
                },
                $inc: {
                  'currentInvest.base': this.mul(closedOrder.side === 'sell' ? -1 : 1, closedOrder.amount).toNumber(),
                  'currentInvest.quote': this.mul(closedOrder.side === 'sell' ? 1 : -1, closedOrder.cost).toNumber(),
                  'fee.base': closedOrder.side === 'sell' ? 0 : this.mul(1, closedOrder.fee.cost).toNumber(),
                  'fee.quote': closedOrder.side === 'sell' ? this.mul(1, closedOrder.fee.cost).toNumber() : 0
                }
              }, {}, (err, doc) => {
                if (err) console.log(err);
              });

            });
          }
        })
      });
    })
  }

  createOrders(bot: Bot) {
    if (bot.orders.length > 0) {
      bot.status = 'error';
      this.updateBot(bot);
      console.log('Something went wrong. there are already orders');
    } else {

      this.dex.fetchOrderBook(bot.symbol).pipe(take(1)).subscribe(book => {
        // const currentSell = book.asks[0][0];
        // const currentBuy = book.bids[0][0];
        // const center = this.div(this.add(currentSell, currentBuy), 2).toNumber();

        let grids = [].concat(bot.grids);
        const closest = grids.reduce((prev, curr) => (Math.abs(this.sub(curr, bot.center)) < Math.abs(this.sub(prev, bot.center)) ? curr : prev));
        grids.splice(grids.indexOf(closest), 1);

        let initialBase = 0;
        let initialQuote = 0;

        grids.forEach((grid, index) => {
          this.dex.createOrder(bot.symbol, 'limit', grid > bot.center ? 'sell' : 'buy', bot.amount, grid, bot.account).pipe(take(1)).subscribe(createdOrder => {
            createdOrder.side = grid > bot.center ? 'sell' : 'buy';
            createdOrder.price = parseFloat(grid);
            createdOrder.amount = bot.amount;
            createdOrder.symbol = bot.symbol;

            bot.orders.push(createdOrder);
            this.updateBot(bot);

            if (createdOrder.side === 'sell') {
              initialBase += bot.amount
            } else {
              initialQuote += this.mul(bot.amount, grid).toNumber();
            }

            if (index === grids.length - 1) {
              bot.status = 'running';
              this.updateBot(bot);

              const historySub = this.getHistory(bot.history_id).pipe(take(1)).subscribe(history => {
                history.initialInvest.base = initialBase;
                history.initialInvest.quote = initialQuote;

                history.currentInvest.base = initialBase;
                history.currentInvest.quote = initialQuote;

                this.updateHistory(history);
                historySub.unsubscribe();
              });

              this.intervals[bot._id] = setInterval(() => {
                this.checkOrders(bot);
              }, this.checkInterval)
            }
          });

        });

      })
    }
  }

  stopBot(bot: Bot) {
    console.log(bot);
    bot.status = 'stopping';
    this.updateBot(bot);

    clearInterval(this.intervals[bot._id]);
    const ordersToDelete = bot.orders;
    ordersToDelete.forEach(toDelete => {
      this.dex.cancelOrder(toDelete.id, toDelete.symbol, bot.account).pipe(take(1)).subscribe(() => {
        bot.orders = bot.orders.filter(order => {
          return toDelete.id !== order.id;
        });
        if (bot.orders.length === 0) {
          bot.status = 'stopped';
        }
        this.updateBot(bot);
      })
    });
  }

  updateBot(bot: Bot) {
    this.bots.update({_id: bot._id}, bot, {}, (err, doc) => {
      if (err) {
        console.log(err);
      } else {
        this.readBots();
      }
    })
  }

  updateHistory(history: History) {
    this.history.update({_id: history._id}, history, {}, (err, doc) => {
      if (err) console.log(err);
    })
  }

  getBots(): Observable<Bot[]> {
    this.readBots();
    return this.subject;
  }

  getBot(id): Observable<Bot> {
    return new Observable(subscriber => {
      this.bots.findOne({_id: id}, (err, doc) => {
        if (err) {
          subscriber.error();
        } else {
          subscriber.next(doc);
        }
        subscriber.complete();
      })
    })
  }

  saveBot(bot: Bot): Observable<Bot> {
    console.log(bot);
    return new Observable(subscriber => {
      this.bots.update({_id: bot._id}, bot, {upsert: true}, (err, num, savedBot: Bot, upsert) => {
        console.log(savedBot, upsert);
        if (err) {
          console.log(err);
          subscriber.error(err);
        } else {
          if (savedBot) {
            subscriber.next(savedBot);
          } else {
            subscriber.next(bot);
          }
          this.readBots();
        }
        subscriber.complete();
      })
    })
  }

  createNewHistoryForBot(bot: Bot): Observable<Bot> {
    return new Observable(subscriber => {
      const history: History = {
        bot_id: bot._id,
        symbol: bot.symbol,
        initialInvest: {base: 0, quote: 0},
        currentInvest: {base: 0, quote: 0},
        fee: {base: 0, quote: 0},
        completedOrders: []
      };
      this.history.insert(history, (err, doc) => {
        if (err) {
          console.log(err);
          subscriber.error(err);
        } else {
          bot.history_id = doc._id;
          this.updateBot(bot);

          this.readBots();
          subscriber.next(bot);
        }
        subscriber.complete();
      })
    });
  }

  delete(bot: Bot) {
    this.bots.remove({_id: bot._id}, (err, doc) => {
      if (err) {
        bot.status = 'error';
        this.updateBot(bot);
      } else {
        this.readBots();
      }
    })
  }

  getHistory(_id: string): Observable<History> {
    return new Observable(subscriber => {
      this.history.findOne({_id: _id}, (err, doc) => {
        if (err) {
          subscriber.error(err);
        } else {
          subscriber.next(doc);
        }
        subscriber.complete();
      })
    })
  }

  add(a, b) {
    a = new Decimal(a);
    b = new Decimal(b);

    return a.add(b);
  }

  sub(a, b) {
    a = new Decimal(a);
    b = new Decimal(b);

    return a.sub(b);
  }

  mul(a, b) {
    a = new Decimal(a);
    b = new Decimal(b);

    return a.mul(b);
  }

  div(a, b) {
    a = new Decimal(a);
    b = new Decimal(b);

    return a.div(b);
  }

  pow(a, pow) {
    a = new Decimal(a);
    return a.pow(pow);
  }

  private readBots() {
    this.bots.find({}, (err, docs) => {
      if (err) this.subject.error(err);
      console.log(docs);
      this.subject.next(docs);
    });
  }
}

