import {Injectable} from '@angular/core';
import {Balances, Exchange, Market, OHLCV, Order, OrderBook, Trade} from 'ccxt';
import {from, Observable} from 'rxjs';
import {ElectronService} from '../../core/services';
import {ApiAccount} from './bots.model';

@Injectable({
  providedIn: 'root'
})
export class CossApiService {

  // @ts-ignore
  dex: Exchange = new ccxt.cossdex({
    'enableRateLimit': true
  });

  constructor(private electronService: ElectronService) {

  }

  fetchMarkets(): Observable<Market[]> {
    return from(this.dex.fetchMarkets());
  }

  fetchOrderBook(symbol: string): Observable<OrderBook> {
    return from(this.dex.fetchOrderBook(symbol))
  }

  fetchTrades(symbol: string): Observable<Trade[]> {
    return from(this.dex.fetchTrades(symbol, undefined, 100));
  }

  fetchOHLCV(symbol, timeframe = '1d'): Observable<OHLCV[]> {
    return from(this.dex.fetchOHLCV(symbol, timeframe, undefined, 500));
  }

  fetchBalance(account: ApiAccount): Observable<Balances> {
    this.dex.apiKey = account.apiKey;
    this.dex.secret = account.secret;
    return from(this.dex.fetchBalance())
  }

  createOrder(symbol, type, side, amount, price, account: ApiAccount): Observable<Order> {
    this.dex.apiKey = account.apiKey;
    this.dex.secret = account.secret;
    return from(this.dex.createOrder(symbol, type, side, amount, price));
  }

  cancelOrder(id, symbol, account: ApiAccount) {
    this.dex.apiKey = account.apiKey;
    this.dex.secret = account.secret;
    return from(this.dex.cancelOrder(id, symbol))
  }

  fetchClosedOrders(symbol, account: ApiAccount): Observable<Array<Order>> {
    this.dex.apiKey = account.apiKey;
    this.dex.secret = account.secret;
    return from(this.dex.fetchClosedOrders(symbol, undefined, 500));
  }

  fetchOpenOrders(symbol, account: ApiAccount): Observable<Array<Order>> {
    this.dex.apiKey = account.apiKey;
    this.dex.secret = account.secret;
    return from(this.dex.fetchOpenOrders(symbol, undefined, 500));
  }

  decimalToPrecision(decimal, type, precision) {
    return this.dex.decimalToPrecision(decimal, type, precision);
  }
}
