import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Market, Trade} from 'ccxt';
import {CossApiService} from '../../services/coss-api.service';
import {take} from 'rxjs/operators';

@Component({
  selector: 'app-trades',
  templateUrl: './trades.component.html',
  styleUrls: ['./trades.component.scss']
})
export class TradesComponent implements OnInit, OnDestroy {

  @Input()
  market: Market;

  trades: Array<Trade>;
  interval;

  constructor(private dex: CossApiService) {
  }

  ngOnInit(): void {
    this.fetchTrades();
    this.interval = setInterval(() => {
      this.fetchTrades()
    }, 60000);
  }

  fetchTrades() {
    this.dex.fetchTrades(this.market.symbol).pipe(take(1)).subscribe(trades => {
      this.trades = trades;
    });
  }

  ngOnDestroy(): void {
    if (this.interval) {
      clearInterval(this.interval)
    }
  }

}
