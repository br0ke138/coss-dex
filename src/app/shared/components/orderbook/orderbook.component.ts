import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ElementRef, EventEmitter,
  Input,
  OnDestroy,
  OnInit, Output,
  ViewChild
} from '@angular/core';
import {Market, OrderBook} from 'ccxt';
import {CossApiService} from '../../services/coss-api.service';
import {take} from 'rxjs/operators';

@Component({
  selector: 'app-orderbook',
  templateUrl: './orderbook.component.html',
  styleUrls: ['./orderbook.component.scss']
})
export class OrderbookComponent implements OnInit, OnDestroy {

  @Input()
  market: Market;

  @Output()
  orderBookChanged: EventEmitter<OrderBook> = new EventEmitter<OrderBook>();

  orderBook: OrderBook;
  interval;

  constructor(private cdr: ChangeDetectorRef, private dex: CossApiService) {
  }

  ngOnInit(): void {
    this.fetchOrderBook();
    this.interval = setInterval(() => {
      this.fetchOrderBook();
    }, 60000);
  }

  fetchOrderBook() {
    this.dex.fetchOrderBook(this.market.symbol).pipe(take(1)).subscribe(orderBook => {
      this.orderBook = orderBook;
      this.orderBookChanged.emit(this.orderBook);
      setTimeout(() => {
        this.scrollToBottom();
      }, 200);
    })
  }

  scrollToBottom(): void {
    try {
      document.getElementById('scrollMe').scrollTop = document.getElementById('scrollMe').scrollHeight;
      this.cdr.detectChanges();
    } catch (err) {
    }
  }

  relDiff(a, b) {
    return 100 * Math.abs((a - b) / ((a + b) / 2));
  }

  ngOnDestroy(): void {
    if (this.interval) {
      clearInterval(this.interval);
    }
  }

}
