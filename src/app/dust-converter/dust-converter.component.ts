import {Component, OnInit} from '@angular/core';
import {CossApiService} from '../shared/services/coss-api.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {debounceTime} from 'rxjs/operators';
import {Balances, Market} from 'ccxt';

@Component({
  selector: 'app-dust-converter',
  templateUrl: './dust-converter.component.html',
  styleUrls: ['./dust-converter.component.scss']
})
export class DustConverterComponent implements OnInit {

  balances: Balances;
  form: FormGroup;
  markets: Array<Market>;

  constructor(private dex: CossApiService) {
  }

  ngOnInit(): void {
    this.dex.fetchMarkets().subscribe(markets => {
      this.markets = markets;
    });

    this.form = new FormGroup({
      apiKey: new FormControl('', [Validators.required]),
      secret: new FormControl('', [Validators.required]),
    });

    this.form.get('apiKey')
      .valueChanges
      .pipe(debounceTime(500))
      .subscribe(apiKey => {
        if (apiKey !== '') {
          this.fetchBalance();
        } else {
          this.balances = null;
        }
      });
  }

  fetchBalance() {
    this.dex.fetchBalance({apiKey: this.form.get('apiKey').value, secret: ''}).subscribe(bal => {
      this.balances = bal;
    });
  }

  convert(asset: string, target: string) {
    this.markets.forEach(market => {
      if (market.symbol === asset + '/' + target) {
        this.dex.createOrder(market.symbol, 'market', 'sell', this.balances[asset].free, undefined, {
          apiKey: this.form.get('apiKey').value,
          secret: this.form.get('secret').value
        }).subscribe(order => {
          setTimeout(() => {
            this.fetchBalance();
          }, 2000);
        }, error => {
          alert('Please check if your secret is correct');
        })
      }
    })
  }

  checkAmount(symbol, free) {
    let found = false;
    this.markets.forEach(market => {
      if (market.symbol === symbol) {
        if (market.limits.amount.min <= free) {
          found = true;
        }
      }
    });
    return found;
  }

}
