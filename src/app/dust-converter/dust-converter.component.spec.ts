import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DustConverterComponent } from './dust-converter.component';

describe('DustConverterComponent', () => {
  let component: DustConverterComponent;
  let fixture: ComponentFixture<DustConverterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DustConverterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DustConverterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
